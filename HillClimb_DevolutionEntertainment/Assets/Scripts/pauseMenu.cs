﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour {

    private bool isPaused = false;
    public GameObject pausePanel;
    public GameObject[] cars;
    private SmoothCamera sc;
    private carPhysics cs;
    private bool coinsAdded = true;
    // Use this for initialization
    void Start () {
        sc = GetComponent<SmoothCamera>();
        cs = cars[PlayerPrefs.GetInt("c")].GetComponent<carPhysics>();
        cars[PlayerPrefs.GetInt("c")].SetActive(true);
        sc.target = cars[PlayerPrefs.GetInt("c")].transform;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
            isPaused = true;
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && isPaused)
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
            isPaused = false;
        }
	}

    public void PauseButtonOn()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;
    }

    public void ResumeButton()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;
    }
    public void ReloadLevel1()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level1");
        carPhysics.coinsAmount = 0;
    }
    public void ReloadLevel2()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level2");
        carPhysics.coinsAmount = 0;
    }
    public void ReloadLevel3()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level3");
        carPhysics.coinsAmount = 0;
    }
    public void ExitToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
        if (coinsAdded)
        {
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + carPhysics.coinsAmount);
            coinsAdded = false;
        }
    }
}

