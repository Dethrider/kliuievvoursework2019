﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Devolution Entertainment 2018-2019.
//Made by Anatolii Kliuiev.
//All rights reserved.
public class menuLoading : MonoBehaviour {

    int time = 300;
    int deltatime = 0;
    // Use this for initialization
    void Start()
    {
     
    }
	// Update is called once per frame
	void Update () {

        if (deltatime == time)
        {
            menuLoad();
        }
        else
        {
           deltatime++;
        }
    }
    void menuLoad()
    {
      SceneManager.LoadScene("MainMenu");
    }
}
