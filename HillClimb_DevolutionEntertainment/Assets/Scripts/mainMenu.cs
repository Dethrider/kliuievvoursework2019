﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainMenu : MonoBehaviour
{

    public Image[] cars;
    private bool isNext = false;
    public GameObject lockedCar;
    public GameObject chooseItButton;
    public GameObject carsPanel;
    public GameObject modificationPanel;
    public GameObject modificationPanel2;
    public GameObject levelChoosingPanel;
    public GameObject isLockedBannerLvl2;
    public GameObject isLockedBannerLvl3;
    private string[] tuning, tuning2;
    private char del = '6';
    public Button[] updateBttns;
    public Button[] updateBttns2;
    public Button lvl2;
    public Button lvl3;
    public Text coinsText;
    public GameObject infoPan;
    public GameObject infoPan2;
    void Awake()
    {
        if (!PlayerPrefs.HasKey("t1"))
        {
            PlayerPrefs.SetString("t1", "06060");
            PlayerPrefs.Save();
        }
        tuning = PlayerPrefs.GetString("t1").Split(del);
        for (int i = 0; i < tuning.Length; i++)
        {
            if (int.Parse(tuning[i]) == 3)
            {
                updateBttns[i].interactable = false;
            }
        }
        if (!PlayerPrefs.HasKey("t2"))
        {
            PlayerPrefs.SetString("t2", "06060");
            PlayerPrefs.Save();
        }
        tuning2 = PlayerPrefs.GetString("t2").Split(del);
        for (int i = 0; i < tuning2.Length; i++)
        {
            if (int.Parse(tuning2[i]) == 3)
            {
                updateBttns[i].interactable = false;
            }
        }
        if (!PlayerPrefs.HasKey("levelUnlock"))
        {
            PlayerPrefs.SetInt("levelUnlock", 1);
            PlayerPrefs.Save();
        }
    }
    // Use this for initialization
    void Start()
    {
        if (!PlayerPrefs.HasKey("coins"))
        {
            PlayerPrefs.SetInt("coins", 0);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("car"))
        {
            PlayerPrefs.SetInt("car", 1);
            PlayerPrefs.Save();
        }
        cars[PlayerPrefs.GetInt("c")].color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        coinsText.text = "x " + PlayerPrefs.GetInt("coins").ToString();
        if (PlayerPrefs.GetInt("car") == 0)
        {
            isLockedBannerLvl3.SetActive(false);
            lockedCar.SetActive(false);
            chooseItButton.SetActive(true);
            lvl3.interactable = true;
        }
    }
    public void carChanger(int car)
    {
        PlayerPrefs.SetInt("c", car);
        PlayerPrefs.Save();
        for (int c = 0; c < cars.Length; c++)
        {
            cars[c].color = Color.black;
            cars[car].color = Color.white;
        }
    }
    public void toModification()
    {
        carsPanel.SetActive(false);
        if (PlayerPrefs.GetInt("c") == 0)
        {
            modificationPanel.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("c") == 1)
        {
            modificationPanel2.SetActive(true);
        }
        lockedCar.SetActive(false);
    }
    public void toLevelChoosing()
    {
        if (PlayerPrefs.GetInt("c") == 0)
        {
            modificationPanel.SetActive(false);
        }
        else if (PlayerPrefs.GetInt("c") == 1)
        {
            modificationPanel2.SetActive(false);
        }
        levelChoosingPanel.SetActive(true);
        if (PlayerPrefs.GetInt("coins") >= 350)
        {
            lvl2.interactable = true;
            isLockedBannerLvl2.SetActive(false);
            PlayerPrefs.SetInt("levelUnlock", 0);
            PlayerPrefs.Save();
        }
        else
        {
            isLockedBannerLvl2.SetActive(true);
            PlayerPrefs.SetInt("levelUnlock", 1);
        }
        if (PlayerPrefs.GetInt("car") == 1)
        {
            isLockedBannerLvl3.SetActive(true);
        }
    }
    public void backToModification()
    {
        if (PlayerPrefs.GetInt("c") == 0)
        {
            modificationPanel.SetActive(true);
            if(PlayerPrefs.GetInt("levelUnlock") == 1)
            {
                isLockedBannerLvl2.SetActive(false);
            }
            if(PlayerPrefs.GetInt("car") == 1)
            {
                isLockedBannerLvl3.SetActive(false);
            }
        }
        else if (PlayerPrefs.GetInt("c") == 1)
        {
            modificationPanel2.SetActive(true);
            if (PlayerPrefs.GetInt("levelUnlock") == 1)
            {
                isLockedBannerLvl2.SetActive(false);
            }
            if (PlayerPrefs.GetInt("car") == 1)
            {
                isLockedBannerLvl3.SetActive(false);
            }
        }
        levelChoosingPanel.SetActive(false);
    }
    public void BackToCarChoosing()
    {
        carsPanel.SetActive(true);
        if (PlayerPrefs.GetInt("c") == 0)
        {
            modificationPanel.SetActive(false);
        }
        else if (PlayerPrefs.GetInt("c") == 1)
        {
            modificationPanel2.SetActive(false);
        }
        if(PlayerPrefs.GetInt("car") == 1)
        lockedCar.SetActive(true);
    }
    public void carTuning(int carPart)
    {

            int updateState = int.Parse(tuning[carPart]) + 1;
            if (updateState <= 3 && 40*updateState <= PlayerPrefs.GetInt("coins"))
            {
                tuning[carPart] = updateState.ToString();
                PlayerPrefs.SetString("t1", tuning[0] + "6" + tuning[1] + "6" + tuning[2]);
                PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - updateState * 40);
                PlayerPrefs.Save();
            }
            if (int.Parse(tuning[carPart]) == 3)
            {
                updateBttns[carPart].interactable = false;
            }
            for (int i = 0; i < tuning.Length; i++)
            {
                print(tuning[i]);
            }
    }
    public void carTuning2(int carPart)
    {
            int updateState = int.Parse(tuning2[carPart]) + 1;
            if (updateState <= 3 && 80 * updateState <= PlayerPrefs.GetInt("coins"))
            {
                tuning2[carPart] = updateState.ToString();
                PlayerPrefs.SetString("t2", tuning2[0] + "6" + tuning2[1] + "6" + tuning2[2]);
                PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - updateState * 80);
                PlayerPrefs.Save();
            }
            if (int.Parse(tuning2[carPart]) == 3)
            {
                updateBttns2[carPart].interactable = false;
            }
            for (int i = 0; i < tuning2.Length; i++)
            {
                print(tuning2[i]);
            }
            
    }
    public void Level1()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level1");
        carPhysics.coinsAmount = 0;
    }
    public void Level2()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level2");
        carPhysics.coinsAmount = 0;
    }
    public void Level3()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level3");
        carPhysics.coinsAmount = 0;
    }
    public void unlockTheCar()
    {
        if (PlayerPrefs.GetInt("coins") >= 1250)
        {
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - 1250);
            PlayerPrefs.SetInt("car", 0);
            PlayerPrefs.Save();
        }
        if (PlayerPrefs.GetInt("car") == 0)
        {
            lockedCar.SetActive(false);
            chooseItButton.SetActive(true);
        }
    }
    public void infoPanelIn()
    {
        if(PlayerPrefs.GetInt("c") == 1)
        infoPan.SetActive(true);
        if (PlayerPrefs.GetInt("c") == 0)
        infoPan2.SetActive(true);
    }
    public void infoPanelExit()
    {
        if (PlayerPrefs.GetInt("c") == 1)
            infoPan.SetActive(false);
        if (PlayerPrefs.GetInt("c") == 0)
            infoPan2.SetActive(false);
    }
}
