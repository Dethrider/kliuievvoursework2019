﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Devolution Entertainment 2018-2019.
//Made by Anatolii Kliuiev.
//All rights reserved.
public class carPhysics : MonoBehaviour {
    WheelJoint2D[] joint2Ds;
    JointMotor2D motorFrontWheel;
    JointMotor2D motorBackWheel;
    public float maxSpeed = -1000f;
    private float backSpeed = 1500f;
    private float acceleration = 800f;
    private float deacceleration = -400f;
    public float breakForce = 3000f;
    private float gravity = 9.8f;
    private float carAngel = 0;
    public float tankerSize;
    private float currentFuel;
    public float fuelUsage;
    public onClick[] carControl;
    public static int coinsAmount;
    private bool isOutOfFuel = false;
    private bool coinsAdded = false;
    private bool isFinished = false;
    private string[] tuning;
    private char del = '6';
    public string keyName;
    public GameObject progressBarFuel;
    public GameObject isOutOfFuelPanel;
    public GameObject isFinishedPanel;
    public GameObject isFinish;
    public Text coinsText;
    public Text textOutOfFuel;
    public Text finishCoins;
    private AudioSource carSound;
    // Use this for initialization
    void Start () {
        joint2Ds = gameObject.GetComponents<WheelJoint2D>();
        motorBackWheel = joint2Ds[0].motor;
        motorFrontWheel = joint2Ds[1].motor;
        currentFuel = tankerSize;
        tuning = PlayerPrefs.GetString(keyName).Split(del);
        switch (PlayerPrefs.GetInt("c"))
        {
            case 0:
                for (int a = 0; a < int.Parse(tuning[0]); a++)
                {
                    maxSpeed -= 500;
                }
                for (int a = 0; a < int.Parse(tuning[1]); a++)
                {
                    tankerSize += 2.5f;
                }
                for (int a = 0; a < int.Parse(tuning[2]); a++)
                {
                    acceleration += 100;
                }
                break;
            case 1:
                for (int a = 0; a < int.Parse(tuning[0]); a++)
                {
                    maxSpeed -= 350;
                }
                for (int a = 0; a < int.Parse(tuning[1]); a++)
                {
                    tankerSize += 3.5f;
                }
                for (int a = 0; a < int.Parse(tuning[2]); a++)
                {
                    acceleration += 200;
                }
                break;
        }
        carSound = GetComponent<AudioSource>();
    }
    void Update()
    {
        coinsText.text = "x " + coinsAmount.ToString();
        
    }
    void FixedUpdate () {
        if (currentFuel <= 0)
        {
            textOutOfFuel.text = coinsText.text;
            isOutOfFuelPanel.SetActive(true);
            Time.timeScale = 0;
            isOutOfFuel = true;
            return;
        }

        carAngel = transform.localEulerAngles.z;
        motorFrontWheel.motorSpeed = motorBackWheel.motorSpeed;
        if(carAngel >= 180)
        {
            carAngel = carAngel - 360;
        }
        if(carControl[0].isClicked == true)
        {
            motorBackWheel.motorSpeed = Mathf.Clamp(motorBackWheel.motorSpeed - (acceleration - gravity * Mathf.PI * (carAngel / 180) * 80) * Time.deltaTime, maxSpeed, backSpeed);
            currentFuel -= fuelUsage * Time.deltaTime;
        }
        if((carControl[0].isClicked == false && motorBackWheel.motorSpeed < 0) || (carControl[0].isClicked == false && motorBackWheel.motorSpeed == 0 && carAngel < 0 ))
        {
            motorBackWheel.motorSpeed = Mathf.Clamp(motorBackWheel.motorSpeed - (deacceleration - gravity * Mathf.PI * (carAngel / 180) * 80) * Time.deltaTime, maxSpeed, 0);
        }
        else if ((carControl[0].isClicked == false && motorBackWheel.motorSpeed > 0) || (carControl[0].isClicked == false && motorBackWheel.motorSpeed == 0 && carAngel > 0))
        {
            motorBackWheel.motorSpeed = Mathf.Clamp(motorBackWheel.motorSpeed - (-deacceleration - gravity * Mathf.PI * (carAngel / 180) * 80) * Time.deltaTime, 0, backSpeed);
        }

        if(carControl[1].isClicked == true && motorBackWheel.motorSpeed > 0)
        {
            motorBackWheel.motorSpeed = Mathf.Clamp(motorBackWheel.motorSpeed - breakForce * Time.deltaTime, 0, backSpeed);
        }
        else if(carControl[1].isClicked == true && motorBackWheel.motorSpeed < 0)
        {
            motorBackWheel.motorSpeed = Mathf.Clamp(motorBackWheel.motorSpeed + breakForce * Time.deltaTime, maxSpeed, 0);
        }
        joint2Ds[0].motor = motorBackWheel;
        joint2Ds[1].motor = motorFrontWheel;
        progressBarFuel.transform.localScale = new Vector2(currentFuel / tankerSize, 1);
        if(currentFuel == 15f)
        {
          
        }
        carSound.pitch = Mathf.Clamp(-motorBackWheel.motorSpeed / 1500, 0.3f, 3);
    }
    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.gameObject.tag == "fuel")
        {
            Destroy(trigger.gameObject);
            currentFuel = tankerSize;
        }
        if(trigger.gameObject.tag == "Finish")
        {
                isFinishedPanel.SetActive(true);
                Time.timeScale = 0;
                finishCoins.text = "x " + coinsAmount.ToString();
        }
    }
}
